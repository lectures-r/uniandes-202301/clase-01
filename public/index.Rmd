---
pagetitle: "Clase 01-Introducción a R"
title: "Taller de R: Estadística y programación"
subtitle: "Clase 01: Introducción a R"
author: 
  name: Eduard Fernando Martínez González
  affiliation: Universidad de los Andes | [ECON-1302](https://bloqueneon.uniandes.edu.co/d2l/home/247211)
output: 
  html_document:
    theme: flatly
    highlight: haddock
    toc: yes
    toc_depth: 4
    toc_float: yes
    keep_md: false
    keep_tex: false ## Change to true if want keep intermediate .tex file
  pdf_document:
    latex_engine: xelatex
    toc: true
    dev: cairo_pdf
    includes:
      in_header: tex/preamble.tex ## For multi-col environments
    pandoc_args:
        --template=tex/mytemplate.tex ## For affiliation field. See: https://bit.ly/2T191uZ
always_allow_html: true
urlcolor: darkblue
mainfont: cochineal
sansfont: Fira Sans
monofont: Fira Code ## Although, see: https://tex.stackexchange.com/q/294362
---

```{r setup, include=F , cache=F}
# Cargar paquetes necesarios
require(pacman)
p_load(knitr,tidyverse,janitor,rio,skimr,kableExtra,ggthemes,fontawesome)

# Configuración del sistema
Sys.setlocale("LC_CTYPE", "en_US.UTF-8")

# Opciones para la visualización de gráficos
options(htmltools.dir.version = F)
opts_chunk$set(fig.align="center", fig.height=4 , dpi=300 , cache=F)
```

## **0. Hoy veremos**

-   **Prólogo**
-   **¿Usar R?**
-   **Configuración inicial**
-   **Interfaz de R y Rstudio**
-   **Primeros pasos en R**

<!--------------------->

<!--------------------->

## **1.Prólogo**

<!--------------------->

### **1.0 Motivación**

Una aproximación a la popularidad de un software es la demanda del lenguaje de programación en el mercado laboral:

```{r echo=FALSE, message=FALSE, warning=FALSE, out.width="60%" , }
pop_df =  data.frame( lang = c("SQL", "Python", "R", "SAS", "Matlab", "SPSS", "Stata"),
                      n_jobs = c(107130, 66976, 48772, 25644, 11464, 3717, 1624),
                      free = c(T, T, T, F, F, F, F))

### Plot it
pop_df %>%  mutate(lang = lang %>% factor(ordered = T)) %>%
            ggplot(aes(x = lang, y = n_jobs, fill = free)) +
            geom_col() + geom_hline(yintercept = 0) +
            aes(x = reorder(lang, -n_jobs), fill = reorder(free, -free)) +
            xlab("Lenguaje estadístico") + scale_y_continuous(label = scales::comma) +
            ylab("Número de trabajos") +
            labs( title = "Número de ofertas de trabajo posteadas en Indeed.com, 2019/01/06") +
            scale_fill_manual( "Código abierto?", labels = c("Sí", "No"), values = c("cadetblue1", "chocolate1")) + theme_minimal() + theme(legend.position = "bottom") 
```

Fuente: [https://github.com/uo-ec607]()

-   Bajamos 5.206 ofertas de empleo para economistas en Colombia posteadas en Computrabajo (vigentes a septiembre de 2022).

![](pics/word_cloud.jpeg){width="50%"}

-   Entre 1.400 y 1.600 requieren que el candidato tenga manejo de bases de datos.

![](pics/freq_jobs.jpeg){width="50%"}

### **1.1 Introducción**

#### **Acerca del curso**

`r fontawesome::fa('book-open')` [Syllabus](https://eduard-martinez.github.io/teaching/r_uniandes/2024-01/syllabus.pdf)

`r fa('github')` Github del curso [aquí](https://github.com/taller-r-202401)

`r fa('clock')` Miércoles 17:00 -- 18:20, ML-208.

#### **Acerca de mí**

`r fa('graduation-cap')` Estudiante doctorado en economía de la Universidad de los Andes

`r fa('globe')` <https://eduard-martinez.github.io>

`r fa('envelope')` [ef.martinezg\@uniandes.edu.co](mailto:ef.martinezg@uniandes.edu.co){.email}

`r fa('github')` <a href="https://github.com/eduard-martinez" style="color:black;"> eduard-martinez </a>

`r fa('twitter')` <a href="https://twitter.com/emartigo" style="color:black;"> @emartigo </a>

<!--------------------->

### **1.2 Objetivo**

Este curso busca promover el uso de lenguajes de programación de código abierto (open source), familiarizando al estudiante con el entorno de trabajo de R e introduciendo a los estudiantes en algunos métodos computacionales aplicados en la investigación económica.

<!--------------------->

### **1.3 Organización del curso**

El curso está dividido en 6 módulos:

-   **Módulo 1:** Fundamentos de programación.
-   **Módulo 2:** Replicabilidad de proyectos.
-   **Módulo 3:** Pre-procesamiento de conjuntos de datos.
-   **Módulo 4:** Bucles y funciones.
-   **Módulo 5:** Datos espaciales.
-   **Módulo 6:** Otras aplicaciones.

<!--------------------->

### **1.4 Evaluación**

#### **Opción 1**

| Componente                    | Peso |
|:------------------------------|-----:|
| Problem set 1 (Módulos 1)     |  25% |
| Problem set 2 (Módulo 2)      |  25% |
| Problem set 3 (Módulos 3 y 4) |  25% |
| Problem set 4 (Módulos 5 y 6) |  25% |
| Bonos:                        |      |

#### **Opción 2**

Se evaluará el contenido de cada clase con un task que se realizará 20 minutos antes de finalizar cada clase. Cada task tendrán un peso de $1/N$ dónde $N$ es el número máximo de talleres desarrolladas en el curso a lo largo del semestre.

**Nota:** [IMPORTANTE](https://economia.uniandes.edu.co/programas/politicas-generales-para-cursos-ofrecidos)

<!--------------------->

### **1.5 Fechas importantes**

-   **Inicio de clases:** 22 de enero de 2024.
-   **Último día para retirar cursos de 16 semanas:** 19 de abril a las 6:00 p. m.

<!--------------------->

### **1.6 Al finalizar este curso:**

![](pics/awesome.jpg){width="70%"}

<!--------------------->
<!--------------------->

## **2. ¿R?**

R es un entorno de programación de código abierto (libre) que fue desarrollado en 1993 por Robert Gentleman y Ross Ihaka del Departamento de Estadística de la Universidad de Auckland.

#### **2.1 Lenguaje orientado a objetos (OOP)**

-   Todo es un objeto.

    -   Puede trabajar con más de una base de datos (objeto) al mismo tiempo (los usuarios de Stata no necesitarán más *keep*, *preserve* o *restore*).

-   Todo tiene un nombre

#### **2.2 Librerías y funciones**

-   Las funciones permiten crear, editar, transformar o eliminar objetos.

-   Las librerías siempre se cargan al iniciar la sesión de R.

-   Puedes usar funciones creadas por otros usuarios.

#### **2.3 Ideal para la ciencia de datos**

-   R y Python son 2 de los lenguajes de programación más populares en la ciencia de datos

    -   Ver: [The Popularity of Data Science Software](http://r4stats.com/articles/popularity/)

-   Cada vez es más frecuente su uso en diferentes industrias (académica, manufacturera,...)

    -   Ver: [The Impressive Growth of R](https://stackoverflow.blog/2017/10/10/impressive-growth-r/)

#### **2.4 Open-source (free!)**

-   Se puede descargar, instalar y utilizar sin coste alguno.

-   R cuenta con 20.229 librerías disponibles en el CRAN. (`r format(Sys.time(), "%Y-%m-%d")`)

<!--------------------->

<!--------------------->

## **3. Configuración inicial**

Antes de iniciar, asegurese de tener instalado `R` y `Rstudio`.

#### **1.1 Descargar [R](https://cran.r-project.org/)**

R es un software de acceso libre usado para el análisis estadístico y creación de gráficos. Funciona en una amplia variedad de plataformas UNIX, Windows y MacOS.

#### **1.2 Descargar [RStudio](https://www.rstudio.com/products/rstudio/download/preview/)**

RStudio es un entorno de desarrollo integrado (IDE) para el lenguaje de programación R. Esta IDE brinda una interfaz más amigable con el usuario facilitando el aprendizaje.

#### **1.3 Instalar R y RStudio**

Puede ir a este [enlace](https://lectures-r.gitlab.io/uniandes-202202/initial-setup) y seguir las instrucciones de instalación para el sistema operativo de su equipo.

<!--------------------->

<!--------------------->

## **4. Interfaz de R y Rstudio**

#### **4.1 Consola de R**

![](pics/interfaz_r.png){width="529"}

#### **4.2. IDE: RStudio**

![](pics/interfaz_rstudio.png){width="541"}

##### **4.2.1 Consola**

La consola permite ejecutar una línea de código y visualizar el resultado de ejecutar un código. Sin embargo, cuando se antepone un ***\#*** a una línea de código, **R** pinta la línea de código sobre la consola pero no la ejecuta.

![](pics/consola.png){width="538"}

Después de ejecutar una línea de código en R, la consola puede retornar un **decoding messages**. Estos pueden ser:

[Warning]() o [message](): sugiere que hay detalles de la función que debemos tener en cuenta (no se detiene la ejecución de la función).

[Error](): se genera cuando ocurrió un error importante causando que la función no continue ejecutándose.

![](pics/decoding.png){width="537"}

##### **4.2.2 Entorno de trabajo**

El entorno de trabajo de R almacena temporalmente los objetos que se asignan durante una sesión. Al momento de cerrar la sesión Rstudio nos preguntará si queremos almacenar en un archivo `.Rdata` los objetos que se encuentran en la memoria activa de R.

![](pics/environment.gif){width="524"}

##### **4.2.3 Editor de sintaxis**

El editor de sintaxis permite escribir las instrucciones que se van a ejecutar en R. Para ejecutar una línea de código del editor de sintaxis se debe sombrear toda la línea de código y se presionan las teclas [Control]() + [Enter]() o se hace clik sobre el boton [run]().

<img src="pics/editor_sintaxis.png" width="514" height="388"/>

<!--=================-->

<!--=================-->

## **5. Primeros pasos en R...**

Antes de empezar a trabajar con objetos, podría familiarizarse con algunas operaciones que puede correr/ejecutar y pintar/imprimir los resultados sobre la consola. Por ejemplo, en `R` se pueden ejecutar una serie de operaciones aritméticas.

#### **5.1 Operaciones aritméticas**

R reconoce todos los operadores aritméticos estándar:

```{r,echo=F}
my_tbl = tribble(
~`Operador aritmético`,~Descripción,
"`+`", "Suma",
"`-`", "Resta",
"`*`", "Multiplicación",
"`/`", "División",
"`^` , `**`", "Exponencial",
"`%%`", "Módulo",
"`%/%`", "División entera",
"`%*%`","Multiplicación matricial",
"`%o%`","Producto exterior",
"`%x%`","Producto Kronecker")

kbl(my_tbl) %>%
kable_paper("striped", full_width = F) %>%
row_spec(0, bold = T, color = "white", background = "#00008B")
```

Puede escribir la operación de suma/resta/producto sobre la consola y pintar los resultados sobre la consola:

```{r}
1+2 ## Suma
5/2 ## Division
2+4*1^3 ## Orden estándar de precedencia (por ejemplo: ejecuta `*` antes que `+`)
```

También podemos invocar operadores de módulo (división de enteros y resto):

```{r}
100 %/% 60 ## ¿Cuántas horas enteras hay en 100 minutos?
100 %% 60 ## ¿Cuántos minutos quedan?
```

#### **5.2 Operadores lógicos**

R también viene equipado con un conjunto completo de operadores lógicos y booleanos, que siguen el protocolo de programación estándar. Por ejemplo:

```{r,echo=F}
my_tbl = tribble(
~`Operador lógico`,~Descripción,
"< , >","Menor y mayor que...",
"<= , >=", "Menor o igual y mayor igual que..." ,
"`==`" , "Igual a..." ,
"`!=`" , "Diferente de..." ,
"&" , "y"  ,
"`|`" , "o"  ,
"`!`" , "Negación")

kbl(my_tbl) %>%
kable_paper("striped", full_width = F) %>%
row_spec(0, bold = T, color = "white", background = "#00008B")
```

Puede ejecutar operaciones lógicas sencillas o compuestas, por ejemplo:

```{r}
1 > 2 ## ¿Es 1 mayor qué 2?
1 > 2 & 1 > 0.5 ## ¿Es 1 mayor qué 2 y menor que 0.5? 
1 > 2 | 1 > 0.5 ## ¿Es 1 mayor qué 2 o menor que 0.5? 
isTRUE (1 < 2)
```

Al igual que la aritmética estándar, las declaraciones lógicas siguen un estricto orden de precedencia. Los operadores lógicos (`>`, `==`, etc.) se evalúan antes que los operadores booleanos (`&` y `|`). No reconocer esto puede conducir a un comportamiento inesperado...

```{r}
1 > 0.5 & 2 # 1 es mayor a 0.5 y mayor a 2?
```

**Solución:** Debe escribir el enunciado lógico de manera correcta:

```{r}
1 > 0.5 & 1 > 2
```

Usamos `!` como abreviatura de negación. Esto será muy útil cuando comencemos a filtrar objetos de datos en función de observaciones no faltantes (es decir, no NA).

```{r}
is.na(1:10)
!is.na(1:10)
# Negate(is.na)(1:10) ## This also works. Try it yourself.
```

#### **5.3. Evaluar coincidencias: `%in%`**

Para ver si un objeto está contenido dentro (es decir, coincide con uno de) una lista de elementos, use `%in%`.

```{r}
4 %in% 1:10
4 %in% 5:10
```

Pronto veremos como asignar objetos en R, por ahora veamos cómo aplicar una evaluación lógica para saber si un objeto es igual a otro. Para ello se emplea `==`:

```{r, error=T}
1 = 1 ## This doesn't work
1 == 1 ## This does.
1 != 2 ## Note the single equal sign when combined with a negation.
```

**Advertencia de evaluación:** números de coma flotante

¿Qué crees que pasará si evaluamos `0.1 + 0.2 == 0.3`?

```{r floating1}
0.1 + 0.2 == 0.3
```

**Problema:** Las computadoras representan números como puntos flotantes binarios (es decir, base 2). Más [aquí](https://floating-point-gui.de/basic/). Esto ayuda a obtener un resultado rápido y eficiente en memoria, pero puede provocar un comportamiento inesperado. Similar a la forma en que la representación decimal estándar (es decir, base 10) no puede capturar con precisión ciertas fracciones (por ejemplo, $\frac{1}{3} = 0.3333...$).

**Solución:** Use `all.equal()` para evaluar flotantes (es decir, fracciones).

```{r floating2}
all.equal(0.1 + 0.2, 0.3)
```
